const encoreLogger = require('@symfony/webpack-encore/lib/logger.js');
const encoreParseRuntime = require('@symfony/webpack-encore/lib/config/parse-runtime.js');
const EncoreWebpackConfig = require('@symfony/webpack-encore/lib/WebpackConfig.js');
const encoreValidateConfig = require('@symfony/webpack-encore/lib/config/validator.js');
const encoreWebpackConfigGenerator = require('@symfony/webpack-encore/lib/config-generator.js');
const encoreProxy = require('@symfony/webpack-encore/lib/EncoreProxy.js');
const fixConfig = require('./fix-config.js');

class EncoreBuilder extends EncoreWebpackConfig {
	constructor(runtimeOptions) {
		super(encoreParseRuntime(runtimeOptions, runtimeOptions.context), true);

		this._ext = {
			compatCheckEnabled: false,
			eslintCompatPluginCallback: null,
			manifestPluginEnabled: false,
			entryFilesPluginEnabled: false,
			entryFilesPluginCallback: null,
			analyzerPluginEnabled: false,
			analyzerPluginCallback: null,
			htmlPluginEnabled: false,
			htmlPluginCallback: null,
			definesCallback: null,
			postCSSPluginsCallback: null
		};
	}

	static fromArgv(argv, cwd) {
		// Swap webtool args scheme: <appEntry> --mode <webpackMode>
		// to encore scheme: <webpackMode> --context <cwd>
		return new EncoreBuilder({
			...argv,
			_: [argv.mode || 'dev'],
			context: cwd || argv.cwd
		});
	}

	configureEnv() {
		encoreLogger.reset();
		if(this.runtimeConfig.outputJson) {
			encoreLogger.quiet();
		}
	}

	isRuntimeEnvironmentConfigured() {
		return true;
	}

	async generateWebpackConfig() {
		encoreValidateConfig(this);
		const webpack = encoreWebpackConfigGenerator(this);
		await fixConfig(this, webpack);
		return webpack;
	}

	proxy() {
		return encoreProxy.createProxy(this);
	}

	// Existing plugins from encore

	enableEntryFilesPlugin(value = true) {
		this._ext.entryFilesPluginEnabled = value;
	}

	configureEntryFilesPlugin(cb) {
		this._ext.entryFilesPluginCallback = cb;
	}

	enableManifestPlugin(value = true) {
		this._ext.manifestPluginEnabled = value;
	}

	// Added plugins

	enableCompatCheck(value = true) {
		this._ext.compatCheckEnabled = value;
	}

	configureEslintCompatPlugin(cb) {
		this._ext.eslintCompatPluginCallback = cb;
	}

	enableAnalyzerPlugin(value = true) {
		this._ext.analyzerPluginEnabled = value;
	}

	configureAnalyzerPlugin(cb) {
		this._ext.analyzerPluginCallback = cb;
	}

	enableHtmlPlugin(value = true) {
		this._ext.htmlPluginEnabled = value;
	}

	configureHtmlPlugin(cb) {
		this._ext.htmlPluginCallback = cb;
	}

	configureDefines(objectOrCb) {
		if(typeof(objectOrCb) === 'function') {
			this._ext.definesCallback = objectOrCb;
		} else if(objectOrCb) {
			const overrides = {...objectOrCb};
			this._ext.definesCallback = defines => Object.assign(defines, overrides);
		} else {
			this._ext.definesCallback = null;
		}
	}

	configurePostCSSPlugins(arrayOrCb) {
		if(typeof(arrayOrCb) === 'function') {
			this._ext.postCSSPluginsCallback = arrayOrCb;
		} else if(arrayOrCb) {
			const plugins = [...arrayOrCb];
			this._ext.postCSSPluginsCallback = () => plugins;
		} else {
			this._ext.postCSSPluginsCallback = null;
		}
	}

	enableNetlifyRedirects(proxiedRoutes = {}) {
		this._ext.netlifyRedirectProxies = proxiedRoutes;
	}
}

module.exports = EncoreBuilder;
