#!/usr/bin/env node

require('v8-compile-cache');
require('dotenv').config();

require('../lib/main.js')(process.argv.slice(2), process.cwd())
	.then(rc => {
		process.exitCode = rc;
	})
	.catch(error => {
		console.error(error);
		process.exitCode = 1;
	});
